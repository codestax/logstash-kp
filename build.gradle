buildscript {
    repositories {
        mavenLocal()
        mavenCentral()
    }
    dependencies {
        classpath 'com.opkloud:kloudprints-gradle-plugin:1.1.10'
    }
}
plugins{
    id 'java'
    id 'maven-publish'
    id 'signing'
//    id 'com.opkloud.kloudprints' version '1.1.10'
}

group 'com.opkloud.kloudprints'
version '1.1.1'
sourceCompatibility = 1.8
apply plugin: 'com.opkloud.kloudprints-plugin'

repositories {
    mavenCentral()
    mavenLocal()
}


dependencies {
    kloudprint 'com.opkloud.kloudprints:default-networking-kp:1.1.0'
}

task sourceJar(type: Jar) {
    classifier "sources"
    from sourceSets.main.allSource
}

task javadocJar(type: Jar, dependsOn: javadoc) {
    classifier "javadoc"
    from javadoc.destinationDir
}

artifacts {
    archives sourceJar
    archives javadocJar
}

signing {
    sign configurations.archives
}

kloudprints {

    param "NetworkStackName:NetworkStack"
    param "ImageId"
    param "InstanceType:t2.large"
    param "KeyName"
    param "stackName:LogstashStack"
    param "awsRegion:us-east-1"
    param "deployment:src/main/resources/logstash"

    dependency "NetworkingKp","com.opkloud.kloudprints:default-networking-kp:1.1.0", [:]

}


publishing {
    publications {
        mavenJava(MavenPublication) {
            customizePom(pom)
            groupId 'com.opkloud.kloudprints'
            artifactId 'logstash-kp'
            version '1.1.1'

            artifact kpPackage

            artifact(sourceJar) {
                classifier = 'sources'
            }
            artifact(javadocJar) {
                classifier = 'javadoc'
            }

            components.java

            pom.withXml {
                def dependenciesNode = asNode().appendNode('dependencies')
                configurations.implementation.allDependencies.each {
                    def dependencyNode = dependenciesNode.appendNode('dependency')
                    dependencyNode.appendNode('groupId', it.group)
                    dependencyNode.appendNode('artifactId', it.name)
                    dependencyNode.appendNode('version', it.version)
                }
            }

            // create the sign pom artifact
            pom.withXml {
                def pomFile = file("${project.buildDir}/generated-pom.xml")
                writeTo(pomFile)
                def pomAscFile = signing.sign(pomFile).signatureFiles[0]
                artifact(pomAscFile) {
                    classifier = null
                    extension = 'pom.asc'
                }
            }

            // create the signed artifacts
            project.tasks.signArchives.signatureFiles.each {
                artifact(it) {
                    def matcher = it.file =~ /-(sources|javadoc)\.jar\.asc$/
                    if (matcher.find()) {
                        classifier = matcher.group(1)
                    } else {
                        classifier = null
                    }
                    extension = 'jar.asc'
                }
            }

        }
    }
    repositories {
        maven {
            url "https://oss.sonatype.org/service/local/staging/deploy/maven2"
            credentials {
                username sonatypeUsername
                password sonatypePassword
            }
        }
    }
}

def customizePom(pom) {
    pom.withXml {
        def root = asNode()

        // eliminate test-scoped dependencies (no need in maven central POMs)
        root.dependencies.removeAll { dep ->
            dep.scope == "test"
        }

        // add all items necessary for maven central publication
        root.children().last() + {
            resolveStrategy = Closure.DELEGATE_FIRST

            description 'Kloudprint that stands up a Logstash cluster.'
            name 'Logstash Kloudprint'
            url 'https://bitbucket.org/codestax/logstash-kp'
            organization {
                name 'com.opkloud'
                url 'http://opkloud.com'
            }
            issueManagement {
                system 'Bitbucket'
                url 'https://bitbucket.org/codestax/logstash-kp/issues'
            }
            licenses {
                license {
                    name 'Apache License 2.0'
                    url 'http://opkloud.com/LICENSE'
                    distribution 'repo'
                }
            }
            scm {
                url 'https://bitbucket.org/codestax/logstash-kp/src'
                connection 'scm:git@bitbucket.org:codestax/logstash-kp.git'
                developerConnection 'scm:git@bitbucket.org:codestax/logstash-kp.git'
            }
            developers {
                developer {
                    name 'Marco Jacobs'
                    email 'marco@codeintelx.com'
                }
                developer {
                    name 'Rashad Moore'
                    email 'rm@codestax.com'
                }
            }
        }
    }
}

model {
    tasks.generatePomFileForMavenJavaPublication {
        destination = file("$buildDir/generated-pom.xml")
        dependsOn kpPackage
    }
    tasks.publishMavenJavaPublicationToMavenLocal {
        dependsOn project.tasks.signArchives
    }
    tasks.publishMavenJavaPublicationToMavenRepository {
        dependsOn project.tasks.signArchives
    }
}


task publoc (dependsOn: [publishToMavenLocal]) {
    // DO NOTHING IN HERE!
}